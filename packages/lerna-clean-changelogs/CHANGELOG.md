# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.0 (2019-02-01)

### Features

- Set default list item format to dashes (not asterisks) to match Prettier ([16c2200](https://gitlab.com/codsen/codsen/commit/16c2200))

## 1.1.0 (2019-01-31)

### Features

- Remove excessive blank lines and convert list item bullets to asterisks ([f53d14b](https://gitlab.com/codsen/codsen/commit/f53d14b))

## 1.0.0 (2019-01-20)

- ✨ First public release
