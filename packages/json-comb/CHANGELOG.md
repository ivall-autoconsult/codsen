# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 0.2.31 (2019-11-20)

### Bug Fixes

- update to the latest meow v4 API ([ca7c1f9](https://gitlab.com/codsen/codsen/commit/ca7c1f9b1e28dd7540442fa19f9ca4b7855b9e34))

## 0.2.0 (2019-06-01)

### Features

- Initial commit ([b7911b7](https://gitlab.com/codsen/codsen/commit/b7911b7))
- Normalisation will fail if a set of only one file is given ([963c3db](https://gitlab.com/codsen/codsen/commit/963c3db))
