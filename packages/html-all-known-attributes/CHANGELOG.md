# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 2.0.0 (2020-04-13)

### Features

- export Set instead of Array and remove JSON source file ([d09e5b7](https://gitlab.com/codsen/codsen/commit/d09e5b7a592ef7b2bd5faef0eef0f7a78038a74f))

### BREAKING CHANGES

- For perf reasons, export Set instead of Array and remove JSON source file

## 1.3.0 (2020-01-05)

### Features

- add xmlns ([7154f5f](https://gitlab.com/codsen/codsen/commit/7154f5f29f3cdc16a8a561eb5724b537300366d4))

## 1.2.0 (2020-01-01)

### Features

- add two html attributes used in edialog tracking links in emails ([4644703](https://gitlab.com/codsen/codsen/commit/46447036e0bfdb5c5357ae510e6ac0e0dce6db75))

## 1.1.0 (2019-12-27)

### Features

- init ([2b332dd](https://gitlab.com/codsen/codsen/commit/2b332dd351aabfe6e284f50eba9a8b45471fbcd3))

## 1.0.0 (2019-12-25)

- ✨ First public release
