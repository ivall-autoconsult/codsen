# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.0 (2019-11-27)

### Bug Fixes

- add requirement for loose array contents object matching - there must be more than 1 obj ([e90ee45](https://gitlab.com/codsen/codsen/commit/e90ee453df8c3924dbaa6401a70824ba9ab03600))
- fix score calculation ([3601ce2](https://gitlab.com/codsen/codsen/commit/3601ce282fb3f186531198ffb61ad41c1bb3e31b))

### Features

- opts.arrayStrictComparison (set to false by default) ([ef8fcdd](https://gitlab.com/codsen/codsen/commit/ef8fcdd63ec2e31a8ed673e56e64f88171ffe275))

## 1.0.0 (2019-11-11)

### Features

- init ([d388611](https://gitlab.com/codsen/codsen/commit/d38861123f7c305e8e34a338fbbfa2c6b1e5a930))
- pass the path as the third argument to cb() ([85574cd](https://gitlab.com/codsen/codsen/commit/85574cd26daf82bb65325529c1d3faa9fd348005))

### BREAKING CHANGES

- initial release

## 1.0.0 (2019-11-10)

- ✨ First public release.
