import { Linter } from "./linter";
import { version } from "../package.json";

export { Linter, version };
