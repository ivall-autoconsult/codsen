# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.0 (2020-04-13)

### Features

- initial scaffolding ([5968866](https://gitlab.com/codsen/codsen/commit/5968866db6702dba9031b7633e3be92eb0d62d5c))
- improve the mismatching quote recognition ([967bd66](https://gitlab.com/codsen/codsen/commit/967bd66ee2a9b4ff9414d1cca715569f5c677ff6))
- improvements to mismatching HTML attribute quote pair recognition ([050dbe0](https://gitlab.com/codsen/codsen/commit/050dbe02569d98c2741aca00fed990004c22eeb2))
- recognise missing tag closing brackets ([2639868](https://gitlab.com/codsen/codsen/commit/26398686dfffafd069fd25577f69009d5a27f2f9))
- recognise repeated (even spaced) equal signs in front of attributes ([a1fa244](https://gitlab.com/codsen/codsen/commit/a1fa2444167c5471e04860af9d3ec15f946a2489))
- starting index is not on a quote; attribute is missing opening quote ([795d3a8](https://gitlab.com/codsen/codsen/commit/795d3a85e3a3a0b46b2ce3f62e93ee3db8f0610e))
