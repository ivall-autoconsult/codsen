# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.0 (2020-04-13)

### Features

- initial scaffolding ([0da2f83](https://gitlab.com/codsen/codsen/commit/0da2f83eac662c8b0f2c82e3dfcfe79f5ef4fd23))

## 1.0.0 (2020-04-12)

- ✨ First public release
