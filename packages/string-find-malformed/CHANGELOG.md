# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.0 (2020-03-16)

### Bug Fixes

- correction to the algorithm ([f1f4a00](https://gitlab.com/codsen/codsen/commit/f1f4a00c2a7dc43fbb13c1eff209beb12dfb0bd9))

### Features

- init ([8199031](https://gitlab.com/codsen/codsen/commit/81990319e699bfc0e3ecf8a7ee38ca8ce46c46a9))
- tweak the matching algorithm to jump to next character if not matched ([f4b0e40](https://gitlab.com/codsen/codsen/commit/f4b0e40729390b950adf7ebc45e01f0d75a34a4a))

## 1.0.0 (2020-03-01)

- ✨ First public release
