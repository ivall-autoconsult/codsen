# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.2 (2020-04-04)

### Bug Fixes

- don't return null when going up from root, return `"0"` ([7743d87](https://gitlab.com/codsen/codsen/commit/7743d877a357afa1ec0452e83b2c507cd927fcfe))

## 1.1.0 (2020-03-16)

### Features

- initial release ([2a59f29](https://gitlab.com/codsen/codsen/commit/2a59f29c3fb4c02d6fd1a439dc6d879b4de6e972))

## 1.0.0 (2020-03-15)

- ✨ First public release.
